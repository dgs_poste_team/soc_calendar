import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ComponentsComunicationService {

  constructor() { }

  dataToBePassed = new Subject<any>();
  dataToBePassedObs = this.dataToBePassed.asObservable();

  changeData(newData) {
    this.dataToBePassed.next(newData);
  }
}
