import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomEventDateSelectorComponent } from './custom-event-date-selector.component';

describe('CustomEventDateSelectorComponent', () => {
  let component: CustomEventDateSelectorComponent;
  let fixture: ComponentFixture<CustomEventDateSelectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomEventDateSelectorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomEventDateSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
