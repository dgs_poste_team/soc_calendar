import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-custom-event-date-selector',
  templateUrl: './custom-event-date-selector.component.html',
  styleUrls: ['./custom-event-date-selector.component.css']
})
export class CustomEventDateSelectorComponent implements OnInit {

  constructor() { }

  @Output() notifyStartEndTime = new EventEmitter<any>();

  en: any;
  startTime: Date;
  endTime: Date;

  invalidDates: Array<Date>

    ngOnInit() {
        let today = new Date();
        let month = today.getMonth();
        let year = today.getFullYear();
        let prevMonth = (month === 0) ? 11 : month -1;
        let prevYear = (prevMonth === 11) ? year - 1 : year;
        let nextMonth = (month === 11) ? 0 : month + 1;
        let nextYear = (nextMonth === 0) ? year + 1 : year;
        /*
        this.minDate = new Date();
        this.minDate.setMonth(prevMonth);
        this.minDate.setFullYear(prevYear);
        this.maxDate = new Date();
        this.maxDate.setMonth(nextMonth);
        this.maxDate.setFullYear(nextYear);
        */

        let invalidDate = new Date();
        invalidDate.setDate(today.getDate() - 1);
        this.invalidDates = [today,invalidDate];
    }

    changeDate() {
      console.log("changeDate() START");

      if(this.startTime != null && this.endTime != null) {
        this.notifyStartEndTime.emit({startDate: this.startTime, endDate: this.endTime});
      }

    }

}
