import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WorkShiftAllocatorComponent } from './work-shift-allocator/work-shift-allocator.component';
import { LoginComponent } from './login/login.component';


const routes: Routes = [
  { path: 'work-shift-allocator', component: WorkShiftAllocatorComponent },
  { path: 'register', component: LoginComponent },
  { path: 'login', component: LoginComponent },
  { path: 'logout', component: WorkShiftAllocatorComponent },
  { path: "**", redirectTo: '/work-shift-allocator' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
