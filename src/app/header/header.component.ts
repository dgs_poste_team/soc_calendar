import { Component, OnInit } from '@angular/core';
import {SidebarModule} from 'primeng/sidebar';
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable } from 'rxjs';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  display;

  isLoggedIn$: Observable<boolean>;
  isLoggedOut$:Observable<boolean>;
  pictureUrl$: Observable<string>;


  constructor(private afAuth: AngularFireAuth) { }

  ngOnInit(): void {
    this.afAuth.authState.subscribe(user => console.log(user));

    this.isLoggedIn$ = this.afAuth.authState.pipe(map(user => !!user));

    this.isLoggedOut$ = this.isLoggedIn$.pipe(map(loggedIn => !loggedIn));

    this.pictureUrl$ =
      this.afAuth.authState.pipe(map(user => user ? user.photoURL: null));

    //console.log("PICTURE: ", this.pictureUrl$.subscribe(pictureUrl => console.log("PICTURE URL INNER: ", pictureUrl)));
  }


  logout() {
    this.afAuth.auth.signOut();
  }

}
