import { TestBed } from '@angular/core/testing';

import { ComponentsComunicationService } from './components-comunication.service';

describe('ComponentsComunicationService', () => {
  let service: ComponentsComunicationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ComponentsComunicationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
