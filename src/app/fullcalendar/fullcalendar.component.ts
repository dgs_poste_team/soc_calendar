import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { EventService } from '../event.service';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import listPlugin from '@fullcalendar/list';
import interactionPlugin, { Draggable } from '@fullcalendar/interaction';
import { FullCalendar } from 'primeng';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {ConfirmationService, Message} from 'primeng/api';
import { ComponentsComunicationService } from '../components-comunication.service';

@Component({
  selector: 'app-fullcalendar',
  templateUrl: './fullcalendar.component.html',
  styleUrls: ['./fullcalendar.component.css']
})
export class FullcalendarComponent implements OnInit {

  events: any[];
  options: any;
  header: any;

  isShiftSame: boolean = false;

  //people: any[];

  @ViewChild('fullcalendar') fullcalendar: FullCalendar;

  calendar:any;
  msgs: Message[] = [];


  constructor(private eventService: EventService,
              private confirmationService: ConfirmationService,
              private componentsComunicationService: ComponentsComunicationService) {}

  ngOnInit() {

    this.componentsComunicationService.dataToBePassedObs.subscribe((data) => {


      if(data.filter) {
        //console.log("Filtered by person: ", data);

        this.eventService.getEventsByPersonName(data.name).subscribe(eventsSnaps => this.getEventMapLogic(eventsSnaps));
      }

      else {
        this.eventService.getEvents().subscribe(eventsSnaps => this.getEventMapLogic(eventsSnaps));
      }

    })

    this.eventService.getEvents().subscribe(eventsSnaps => this.getEventMapLogic(eventsSnaps));


    this.options = {
        plugins:[ dayGridPlugin, timeGridPlugin, interactionPlugin, listPlugin ],
        defaultDate: '2017-02-01',
        header: {
            left: 'prev,next',
            center: 'title',
            right: 'dayGridMonth,timeGridWeek,timeGridDay'
        },
        editable: true,
        nextDayThreshold: '09:00:00',
        allDayDefault: false,

        dateClick: (dateClickEvent) =>  {         // <-- add the callback here as one of the properties of `options`
          console.log("DATE CLICKED !!!");
        },

        eventClick: (eventClickEvent) => {
          console.log("EVENT CLICKED !!!");
          console.log(eventClickEvent.event.id);

          this.confirmationService.confirm({
            message: 'Delete work shift. Are you sure that you want to proceed?',
            header: 'Confirmation',
            icon: 'pi pi-exclamation-triangle',
            accept: () => {
                this.msgs = [{severity:'info', summary:'Confirmed', detail:'You have accepted'}];
                this.eventService.removeEventById(eventClickEvent.event);
            },
            reject: () => {
                this.msgs = [{severity:'info', summary:'Rejected', detail:'You have rejected'}];
            }
          });


          //this.eventService.removeEventById(eventClickEvent.event);




        },

        eventDragStop: (eventDragStopEvent) => {
          console.log("EVENT DRAG STOP !!!");
        },

        eventReceive: (eventReceiveEvent) => {
          //console.log("eventReceiveEvent: ", eventReceiveEvent);
          //console.log("events: ", this.events);

          if(this.events.length != 0 &&
             this.events.some(e => e.title === eventReceiveEvent.event.title) &&
             this.events.some(e => e.start === this.fromDateToString(eventReceiveEvent.event.start))
             )
          {
            this.isShiftSame = true;
            //console.log("isShiftSame: " + this.isShiftSame);
            return;
          }

          else {
            this.isShiftSame = false;
            //console.log("isShiftSame: " + this.isShiftSame);

            eventReceiveEvent.event.setAllDay(false, {maintainDuration: true});
            this.eventService.addEvent(eventReceiveEvent);
          }
        }
    };

  }

  ngAfterViewInit() {

    this.calendar = this.fullcalendar.getCalendar();
    console.log("CALENDAR: " + this.calendar);

  }

  /**
   * Convert a date into a valid formatted string:
   */
  fromDateToString(date: Date): String {
    date = new Date(+date);
    date.setTime(date.getTime() - date.getTimezoneOffset() * 60000);
    let dateAsString =  date.toISOString().substr(0, 19);
    return dateAsString;
  }

  getEventMapLogic(eventsSnaps) {

    this.events = eventsSnaps.map(currentEventSnap => {
      //console.log("DOCUMENT ID: ", currentCourseSnap.payload.doc.id);
      //console.log("DATA: ", currentCourseSnap.payload.doc.data());
      var currentEvent = {
        id: currentEventSnap.payload.doc.id,
        ...currentEventSnap.payload.doc.data()
      };

      var date = currentEventSnap.payload.doc.data().start.toDate()
      var hour = date.getHours();

      var startDateAsString = this.fromDateToString(date);

      console.log("startDateAsString: ", startDateAsString);
      console.log("hour: ", hour);

      currentEvent.start = startDateAsString;

      if(hour === 7) {
        currentEvent['backgroundColor'] = 'red';
      }
      else if(hour === 15) {
        currentEvent['backgroundColor'] = 'green';
      }
      else if(hour === 23) {
        currentEvent['backgroundColor'] = 'black';
      }

      return currentEvent;
    });

    console.log("EVENTS 123: ", this.events);

  }



}




