import { Injectable } from '@angular/core';
//import { Http } from '@angular/http';
import { HttpClientModule, HttpClient } from '@angular/common/http'
import { BehaviorSubject, Observable } from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';
import * as firebase from 'firebase';


@Injectable()
export class EventService {

  //private events = [];

  private events = [
    {id: 1, title: 'All Day Event', start: '2017-02-01'},
    {id: 2, title: 'Long Event', start: '2017-02-07', end: '2017-02-10'},
    {id: 3, title: 'Repeating Event', start: '2017-02-09T16:00:00'},
    {id: 3, title: 'test', start: '2017-02-20T07:00:00'},
  ];


  private people = [
    {id: 1, name: "PERSONA 1"},
    {id: 2, name: "PERSONA 2"},
    {id: 3, name: "PERSONA 3"},
    {id: 4, name: "PERSONA 4"},
    {id: 5, name: "PERSONA 5"},
  ];

  items: Observable<any[]>;

  public eventData = new BehaviorSubject(this.events);

  constructor(private http: HttpClient,
              private db: AngularFirestore) {}


  /**
   * Return the list of all the work shift related to all the person in the calendar:
   */
  /*
  getEvents(): Observable<any[]> {
    this.items = this.db.collection('calendar').valueChanges();

    return this.items;
  }
  */

 getEvents(): Observable<any[]> {
  this.items = this.db.collection('calendar').snapshotChanges();

  return this.items;
}





  /**
   * Return the list of all the work shift related to a single specific person in the calendar:
   * @param personName: the name of the person for which I want retrieve work
   */
  getEventsByPersonName(personName: String): Observable<any[]> {
    console.log("getEventsByPersonName() START !!!");
    this.items = this.db.collection('calendar', ref => ref.where('title', '==', personName)).snapshotChanges();

    console.log("getEventsByPersonName() END !!!");
    return this.items;

  }


  addEvent(event) {
    console.log(event.event.end);
    var newEvent = {id: 5, title: event.event.title, start: event.event.start, end: event.event.end};

    var docUID = "";

    this.db
        .collection("calendar")
        .add({ title: newEvent.title,
               start: firebase.firestore.Timestamp.fromDate(newEvent.start),
               end: firebase.firestore.Timestamp.fromDate(newEvent.end)
             })
        .then(function(docRef) {
          docUID = docRef.id;
          console.log(" event successfully written! DOC UID: " + docUID);
          newEvent["uid"] = docUID;
          console.log("newEvent: ", newEvent);

          //this.events.push(newEvent);
          //this.eventData.next([...this.events]);

          console.log("START: " + event.event.start);
          var startDateTime = new Date(event.event.start)
          console.log("START DATETIME: " + startDateTime);
        })
        .catch(function(error) {
          console.error("Error writing document event: ", error);
        });

        event.event.remove();



  }


  /**
   * Remove an event from the calendar.
   * @param event The event that have to be removed
   */
  //removeEventById(event:any): Observable<any[]>  {
    removeEventById(event:any) {
    console.log("EVENT TO BE DELETED: ", event);

    this.db.collection("calendar").doc(event.id).delete().then(function() {
      console.log("Document successfully deleted!");
    }).catch(function(error) {
      console.error("Error removing document: ", error);
    });
    /*
    this.events=this.events.filter(each=>each.id!=event.id);
    //console.log("EVENTS: ",  this.events);
    //event.remove();
    let eventData = new BehaviorSubject(this.events);
    this.eventData.next(this.events);
    return this.eventData.asObservable();
    */
  }


  getPeople(): Promise<any[]> {
    return Promise.all(this.people)
        .then(res => {
          console.log(res);
          return res;
        })
  }

}
