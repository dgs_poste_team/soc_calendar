import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkShiftAllocatorComponent } from './work-shift-allocator.component';

describe('WorkShiftAllocatorComponent', () => {
  let component: WorkShiftAllocatorComponent;
  let fixture: ComponentFixture<WorkShiftAllocatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkShiftAllocatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkShiftAllocatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
