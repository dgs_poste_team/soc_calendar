import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import { PeopleListComponent } from "./people-list.component"
import { EventService } from '../event.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('people-list', () => {
    let component: PeopleListComponent;
    let fixture: ComponentFixture<PeopleListComponent>;
    let eventServiceSpy : jasmine.SpyObj<EventService>;
        beforeEach(async(() => {
          const eventServiceSpyObj = jasmine.createSpyObj('EventService',['getPeople'])

          TestBed.configureTestingModule({
              schemas:[NO_ERRORS_SCHEMA],
              declarations: [PeopleListComponent],
              imports: [HttpClientTestingModule],
              providers : [{ provide : EventService, useValue : eventServiceSpyObj },
              ]

        });

        // Create a testing version of my PeopleListComponent:
        fixture = TestBed.createComponent(PeopleListComponent);
        //eventServiceSpy = TestBed.inject(EventService);
        eventServiceSpy = <jasmine.SpyObj<EventService>><any>TestBed.inject(EventService);
        component = fixture.componentInstance;
        //fixture.detectChanges();
    }));

    it('createEventObject()  return 1', () => {
        console.log("TEST 1 START");
        var methodOutput = component.createEventObject();
        console.log("METHOD OUTPUT: " + methodOutput);
        //expect(methodOutput).toBe(1);
        expect(methodOutput).toEqual({parola: "stocazzo"});
    })
})
