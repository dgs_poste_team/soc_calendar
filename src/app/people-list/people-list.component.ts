import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { EventService } from '../event.service';
import interactionPlugin, { Draggable } from '@fullcalendar/interaction';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { CustomEventDateSelectorComponent } from '../custom-event-date-selector/custom-event-date-selector.component';
import { EventHandlerVars } from '@angular/compiler/src/compiler_util/expression_converter';
import { ComponentsComunicationService } from '../components-comunication.service';

interface WorkShiftTypes {
  name: string;
  value: string;
}


@Component({
  selector: 'app-people-list',
  templateUrl: './people-list.component.html',
  styleUrls: ['./people-list.component.css']
})
export class PeopleListComponent implements OnInit {

  people: any[];
  workShiftTypes: WorkShiftTypes[];
  selectedShift: WorkShiftTypes;

  customEventStartingDate;
  customEventDuration;

  isFilterByPersonActive: boolean = false;


  @ViewChild('draggable_people') draggablePeopleExternalElement: ElementRef;

  constructor(private eventService: EventService,
              private componentsComunicationService: ComponentsComunicationService) { }

  ngOnInit(): void {
    this.eventService.getPeople().then(people => {this.people = people;});

    this.selectedShift = {name: 'Mattina', value: 'morning'};

    this.workShiftTypes = [
      {name: 'Mattina', value: 'morning'},
      {name: 'Pomeriggio', value: 'afternoon'},
      {name: 'Notte', value: 'night'},
      {name: 'Custom', value: 'custom'}
    ];
  }

  ngAfterViewInit() {
    console.log("PEOPLE LIST ngAfterViewInit() START !!!")
    var self = this

    new Draggable(this.draggablePeopleExternalElement.nativeElement, {
      itemSelector: '.fc-event',
      eventData: function(eventEl) {
        console.log("DRAG !!!");
        console.log("SELECTED SHIFT: " + self.selectedShift.value);
        console.log("DRAGGABLE OBJECT: " + self.draggablePeopleExternalElement.nativeElement);

        let returnedEvent = self.createEventObject(self.selectedShift.value, eventEl.innerText);

        return returnedEvent;
      }
    });

  }

  /**
   * Create a valid event to be inserted into the calendar using the chosen information:
   * @param selectedShift: it reprsent the work shift with start and end time.
   * @param title : it is the name of the person working in the selected time shift.
   */
  createEventObject(selectedShift, title) {
    console.log("createEventObject() START");

    var start_event = null;
    var end_event = null;
    var event_duration = 8;

    if(selectedShift == 'morning')
      start_event = "07:00";
    else if(selectedShift == 'afternoon')
      start_event = "15:00";
    else if(selectedShift == 'night')
      start_event = "23:00";
    else if (selectedShift == 'custom') {
      start_event = this.customEventStartingDate;
      event_duration = this.customEventDuration;
    }

    var event = {
      title: title,
      startTime: start_event,
      duration: { hours: event_duration }
      //color: '#7FFF00'
    };

    return event;
  }


  /**
   * Retrieve the start and end date fired by the event inside the child component used to choose the
   * custom shift starting\ending time. Check if these values are valid and in case calculate the
   * event duration.
   */
  onChangeCustomShiftDate(event) {
    console.log("onChangeCustomShiftDate() START");
    console.log("START DATE: " + event.startDate);
    console.log("END DATE: " + event.endDate);

    // If the event start date is < of the event end date the work shift is ok:
    if(event.startDate.getHours() < event.endDate.getHours()) {

      let workShiftDuration = event.endDate.getHours() - event.startDate.getHours();
      console.log("workShiftDuration: " + workShiftDuration);

      this.customEventStartingDate = event.startDate.getHours();
      this.customEventDuration = workShiftDuration;
    }
    else {

    }

  }


}
