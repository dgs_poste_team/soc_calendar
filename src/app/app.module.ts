import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FullCalendarModule } from 'primeng/fullcalendar';

import { HttpClientModule } from '@angular/common/http'
import { FullcalendarComponent } from './fullcalendar/fullcalendar.component';
import { EventService } from './event.service';
import { PeopleListComponent } from './people-list/people-list.component';

import { FormsModule } from '@angular/forms';
import { DropdownModule, OrderListModule, CalendarModule, ConfirmDialog, ConfirmDialogModule, ConfirmationService, DialogModule, SidebarModule } from 'primeng';
import {SelectButtonModule} from 'primeng/selectbutton';
import { CommonModule } from '@angular/common';
import { CustomEventDateSelectorComponent } from './custom-event-date-selector/custom-event-date-selector.component';
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
import {AngularFireAuthModule} from '@angular/fire/auth';
import {AngularFirestoreModule} from '@angular/fire/firestore';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { LoginComponent } from './login/login.component';
import { WorkShiftAllocatorComponent } from './work-shift-allocator/work-shift-allocator.component';
import {AngularFireStorageModule} from '@angular/fire/storage';
import { PeopleListItemComponent } from './people-list-item/people-list-item.component';







@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FullcalendarComponent,
    PeopleListComponent,
    CustomEventDateSelectorComponent,
    LoginComponent,
    WorkShiftAllocatorComponent,
    PeopleListItemComponent
  ],
  imports: [
    FormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    DropdownModule,
    OrderListModule,
    SelectButtonModule,
    CalendarModule,
    ConfirmDialogModule,
    AppRoutingModule,
    FullCalendarModule,
    HttpClientModule,
    CommonModule,
    DialogModule,
    SidebarModule,
    FontAwesomeModule,


    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    AngularFirestoreModule,
    AngularFireStorageModule,


    FontAwesomeModule

  ],
  providers: [EventService, ConfirmationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
