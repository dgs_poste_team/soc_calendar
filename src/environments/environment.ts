// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBYjh3o5pFirul19QKRgBaRoJIN3pJ3hBc",
    authDomain: "soc-dashboard.firebaseapp.com",
    databaseURL: "https://soc-dashboard.firebaseio.com",
    projectId: "soc-dashboard",
    storageBucket: "soc-dashboard.appspot.com",
    messagingSenderId: "801320773797",
    appId: "1:801320773797:web:14e69306da7b0838d4c54c",
    measurementId: "G-BHVTXG6CY4"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
